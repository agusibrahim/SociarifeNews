
package id.rdi.sociarifenews.model.news.newspojo;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomFields implements Parcelable
{

    @SerializedName("dfiFeatured")
    @Expose
    private List<String> dfiFeatured = null;
    public final static Parcelable.Creator<CustomFields> CREATOR = new Creator<CustomFields>() {


        @SuppressWarnings({
            "unchecked"
        })
        public CustomFields createFromParcel(Parcel in) {
            return new CustomFields(in);
        }

        public CustomFields[] newArray(int size) {
            return (new CustomFields[size]);
        }

    }
    ;

    protected CustomFields(Parcel in) {
        if (in.readByte() == 0x01) {
            dfiFeatured = new ArrayList<String>();
            in.readList(dfiFeatured, String.class.getClassLoader());
        } else {
            dfiFeatured = null;
        }
    }

    public CustomFields() {
    }

    public List<String> getDfiFeatured() {
        return dfiFeatured;
    }

    public void setDfiFeatured(List<String> dfiFeatured) {
        this.dfiFeatured = dfiFeatured;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (dfiFeatured == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(dfiFeatured);
        }
    }
    public int describeContents() {
        return  0;
    }

}

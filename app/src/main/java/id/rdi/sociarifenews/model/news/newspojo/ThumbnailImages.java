
package id.rdi.sociarifenews.model.news.newspojo;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ThumbnailImages implements Parcelable
{

    @SerializedName("full")
    @Expose
    private Full_ full;
    @SerializedName("thumbnail")
    @Expose
    private Thumbnail_ thumbnail;
    @SerializedName("medium")
    @Expose
    private Medium_ medium;

    @Expose
    private ZerifOurTeamPhoto_ zerifOurTeamPhoto;
    public final static Parcelable.Creator<ThumbnailImages> CREATOR = new Creator<ThumbnailImages>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ThumbnailImages createFromParcel(Parcel in) {
            return new ThumbnailImages(in);
        }

        public ThumbnailImages[] newArray(int size) {
            return (new ThumbnailImages[size]);
        }

    }
    ;

    protected ThumbnailImages(Parcel in) {
        this.full = ((Full_) in.readValue((Full_.class.getClassLoader())));
        this.thumbnail = ((Thumbnail_) in.readValue((Thumbnail_.class.getClassLoader())));
        this.medium = ((Medium_) in.readValue((Medium_.class.getClassLoader())));
        this.zerifOurTeamPhoto = ((ZerifOurTeamPhoto_) in.readValue((ZerifOurTeamPhoto_.class.getClassLoader())));
    }

    public ThumbnailImages() {
    }

    public Full_ getFull() {
        return full;
    }

    public void setFull(Full_ full) {
        this.full = full;
    }

    public Thumbnail_ getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Thumbnail_ thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Medium_ getMedium() {
        return medium;
    }

    public void setMedium(Medium_ medium) {
        this.medium = medium;
    }


    public ZerifOurTeamPhoto_ getZerifOurTeamPhoto() {
        return zerifOurTeamPhoto;
    }

    public void setZerifOurTeamPhoto(ZerifOurTeamPhoto_ zerifOurTeamPhoto) {
        this.zerifOurTeamPhoto = zerifOurTeamPhoto;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(full);
        dest.writeValue(thumbnail);
        dest.writeValue(medium);
        dest.writeValue(zerifOurTeamPhoto);
    }

    public int describeContents() {
        return  0;
    }

}


package id.rdi.sociarifenews.model.news.newspojo;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ZerifPostThumbnailLargeMobile_ implements Parcelable
{

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("width")
    @Expose
    private Integer width;
    @SerializedName("height")
    @Expose
    private Integer height;
    public final static Parcelable.Creator<ZerifPostThumbnailLargeMobile_> CREATOR = new Creator<ZerifPostThumbnailLargeMobile_>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ZerifPostThumbnailLargeMobile_ createFromParcel(Parcel in) {
            return new ZerifPostThumbnailLargeMobile_(in);
        }

        public ZerifPostThumbnailLargeMobile_[] newArray(int size) {
            return (new ZerifPostThumbnailLargeMobile_[size]);
        }

    }
    ;

    protected ZerifPostThumbnailLargeMobile_(Parcel in) {
        this.url = ((String) in.readValue((String.class.getClassLoader())));
        this.width = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.height = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    public ZerifPostThumbnailLargeMobile_() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(url);
        dest.writeValue(width);
        dest.writeValue(height);
    }

    public int describeContents() {
        return  0;
    }

}

package id.rdi.sociarifenews;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabItem;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import id.rdi.sociarifenews.adapter.NewsAdapter;
import id.rdi.sociarifenews.model.news.catpojo.Category;
import id.rdi.sociarifenews.model.news.catpojo.Category_;
import id.rdi.sociarifenews.model.news.newspojo.News;
import id.rdi.sociarifenews.model.news.newspojo.Post;

public class MainActivity extends AppCompatActivity {
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private View appBar, appbarGradient;
    public static String NEWS_HOST="http://agusibrah.im/";
    private static AsyncHttpClient httpClient;
    private SharedPreferences prefCat;
    private List<Category_> news_category=new ArrayList<>();
    private TabLayout tabLayout;
    private static GsonBuilder gsonb;
    private static LinearLayoutManager lmgr;
    private static HashMap<Integer, List<Post>> news_list_data_map=new HashMap<>();
    private static HashMap<Integer, Boolean> news_list_loaded_map=new HashMap<>();
    private static HashMap<Integer, Integer> news_list_page_map=new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        httpClient=new AsyncHttpClient();
        gsonb = new GsonBuilder();
        prefCat=getSharedPreferences("news_category", MODE_PRIVATE);
        Toolbar toolbar =findViewById(R.id.toolbar);
        appBar=findViewById(R.id.appbar);
        tabLayout = findViewById(R.id.tabs);
        setSupportActionBar(toolbar);
        toolbar.setTitle("News");
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), news_category);
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        if(prefCat.getString("category",  null)!=null){
            loadTabs(prefCat.getString("category",""));
        }else initCategory(true);
    }

    private void initCategory(boolean settabs){
        httpClient.get(NEWS_HOST + "?json=get_category_index", new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(MainActivity.this, "Load failed, "+throwable.getMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                SharedPreferences.Editor ed = prefCat.edit();
                ed.putString("category", responseString);
                ed.putLong("category_last_update", new Date().getTime());
                ed.commit();
                news_category.clear();
                tabLayout.removeAllTabs();
                if(settabs) loadTabs(responseString);
            }
        });
    }

    private void loadTabs(String responseString) {
        Gson gson=gsonb.create();
        Category cats= gson.fromJson(responseString, Category.class);
        news_category.addAll(cats.getCategories());
        mSectionsPagerAdapter.notifyDataSetChanged();
        for(Category_ category:cats.getCategories()){
            news_list_data_map.put(category.getId(), new ArrayList<>());
            news_list_loaded_map.put(category.getId(), false);
            news_list_page_map.put(category.getId(), 1);
            tabLayout.addTab(tabLayout.newTab().setText(category.getTitle().toUpperCase()));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static class PlaceholderFragment extends Fragment {
        private RecyclerView news_list;
        private Category_ category;
        //private List<Post> news_list_data=new ArrayList<>();
        private NewsAdapter news_adapter;


        public PlaceholderFragment() {
        }
        public static PlaceholderFragment newInstance(int sectionNumber, Category_ category) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt("section_number", sectionNumber);
            args.putParcelable("category", category);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            Bundle args = getArguments();
            category=args.getParcelable("category");
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            news_list=rootView.findViewById(R.id.news_list);
            news_list.setLayoutManager(lmgr= new LinearLayoutManager(getActivity()));
            news_list.setAdapter(news_adapter= new NewsAdapter(news_list_data_map.get(category.getId()), getActivity()));
            return rootView;
        }

        @Override
        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            if(news_adapter.getItemCount()<=0) loadNews(news_list_page_map.get(category.getId()), category.getId());
            news_adapter.setOnNewsClickListener((post, position, holdr) -> {
                Intent intent=new Intent(getActivity(), NewsReader.class);
                intent.putExtra("post", post);
                startActivity(intent);
            });
            news_adapter.setOnLoadMoreListener((position, count) -> {
                if(!news_list_loaded_map.get(category.getId())){
                    news_list_page_map.put(category.getId(), news_list_page_map.get(category.getId())+1);
                    loadNews(news_list_page_map.get(category.getId()), category.getId());
                    Toast.makeText(getActivity(), category.getTitle()+" Load more... "+news_list_page_map.get(category.getId()),Toast.LENGTH_SHORT).show();
                    news_list_loaded_map.put(category.getId(), true);
                }
            });
        }

        private void loadNews(int page, int categoryid) {
            //httpClient.get(NEWS_HOST + "?json=get_recent_posts&page="+page+"&count=10"+(categoryid>-1?"&category_id="+categoryid:""), new TextHttpResponseHandler() {
            httpClient.get(NEWS_HOST + "?json=get_category_posts&page="+page+"&count=10&category_id="+categoryid, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Toast.makeText(getActivity(), "Failed load new list, "+throwable.getMessage(), Toast.LENGTH_LONG).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    try {
                        JSONObject job=new JSONObject(responseString);
                        String posts=job.getString("posts");
                        Post[] news=gsonb.create().fromJson(posts, Post[].class);
                        for(int i=0;i<news.length;i++){
                            Post pos = news[i];
                            pos.isLarge=Math.random() < 0.5;
                            news_list_data_map.get(category.getId()).add(pos);
                        }
                        news_adapter.notifyDataSetChanged();
                        if(news.length>=10)
                            news_list_loaded_map.put(category.getId(), false);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        List<Category_> cats;
        public SectionsPagerAdapter(FragmentManager fm, List<Category_> cats) {
            super(fm);
            this.cats=cats;
        }
        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(position, cats.get(position));
        }

        @Override
        public int getCount() {
            return cats.size();
        }
    }

    @Override
    protected void onDestroy() {
        news_list_data_map.clear();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        initCategory(false);
        super.onBackPressed();
    }
}

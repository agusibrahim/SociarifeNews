package id.rdi.sociarifenews;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.ethanhua.skeleton.Skeleton;
import com.ethanhua.skeleton.ViewSkeletonScreen;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.pierfrancescosoffritti.youtubeplayer.player.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerFullScreenListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerView;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;
import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import org.json.JSONException;
import org.json.JSONObject;

import cn.jzvd.JZVideoPlayerStandard;
import cz.msebera.android.httpclient.Header;
import id.rdi.sociarifenews.model.news.newspojo.Category;
import id.rdi.sociarifenews.model.news.newspojo.Post;

/**
 * Created by RSOS on 12/11/2017.
 */

public class NewsReader extends AppCompatActivity {
    private ImageView mNewsreaderPoster;
    private TextView mNewsreaderTitle;
    private TextView mNewsreaderDate;
    private WebView mNewsreaderContent;
    private Bundle extras;
    private Post current_post;
    private AsyncHttpClient httpClient;
    private GsonBuilder gsonb;
    private DisplayMetrics dm;
    private Toolbar toolbar;
    private YouTubePlayerView youTubePlayerView;
    private FullScreenManager fullScreenManager;
    //private View toolbarBlur;
    private NestedScrollView nsv;
    private View skeletonView;
    private ViewSkeletonScreen skeletonFadeNews;
    private Toolbar mToolbar;
    private YouTubePlayerView mYoutubePlayerView;
    private LinearLayout mNewsSkeleton;
    private StateButton mNewsGotoOriginallink;
    private NestedScrollView mNewsreaderNsv;
    private View mNewsreaderCommentGrad;
    private ImageView mNewsCommentSend;
    private ImageView mNewsreaderShare;
    private FrameLayout mNewsreaderComment;
    private ImageView mNewsreaderSpeak;
    private CoordinatorLayout mMainContent;
    private View mCommentBar;
    private TextView mNewsCommentCount;
    private View mNewsGoUp;
    private AppBarLayout appBar;
    private EditText mCommentInput;
    private View posterLoading;
    private JZVideoPlayerStandard mGifPlayer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_reader_parent);
        httpClient = new AsyncHttpClient();
        gsonb = new GsonBuilder();
        initView();
        //StatusBarUtil.immersive(this);
        dm = getResources().getDisplayMetrics();
        fullScreenManager = new FullScreenManager(this);
        extras = getIntent().getExtras();
        setSupportActionBar(toolbar);
        setTitle("NewsReader");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //StatusBarUtil.setPaddingSmart(this, toolbar);
        //StatusBarUtil.setPaddingSmart(this, toolbarBlur);
        //StatusBarUtil.setPaddingSmart(this, nsv);
        current_post = extras.getParcelable("post");
        mNewsreaderTitle.setText(current_post.getTitle());
        mNewsreaderDate.setText(current_post.getDate());
        skeletonFadeNews = Skeleton.bind(skeletonView)
                .load(R.layout.news_content_skeleton)
                .show();
        mNewsreaderContent.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                skeletonView.setVisibility(View.GONE);
                skeletonFadeNews.hide();
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            mNewsreaderContent.setWebContentsDebuggingEnabled(true);
        }
        mNewsreaderContent.getSettings().setJavaScriptEnabled(true);
        mNewsreaderContent.setHorizontalScrollBarEnabled(false);
        mNewsreaderContent.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        loadNews();
        if (isVideoPost(current_post)) {
            youTubePlayerView.setVisibility(View.VISIBLE);
            mNewsreaderPoster.setVisibility(View.GONE);
            youTubePlayerView.initialize(initializedYouTubePlayer -> initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                @Override
                public void onReady() {
                    String videoId = current_post.getContentImages().get(0).split("ytimg.com/vi/")[1].split("/")[0];
                    initializedYouTubePlayer.loadVideo(videoId, 0);
                    initFullScreenListener(initializedYouTubePlayer);
                }
            }), true);
        }else if(isGifPost(current_post)){
            if(current_post.getContentImages().size()<2) return;
            youTubePlayerView.setVisibility(View.GONE);
            mNewsreaderPoster.setVisibility(View.GONE);
            mGifPlayer.setVisibility(View.VISIBLE);
            mNewsGotoOriginallink.setVisibility(View.GONE);
            mGifPlayer.setUp(current_post.getContentImages().get(1),JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL, "");
            mGifPlayer.loop=true;
            mGifPlayer.progressBar.setVisibility(View.GONE);
            mGifPlayer.totalTimeTextView.setVisibility(View.GONE);
            mGifPlayer.currentTimeTextView.setVisibility(View.GONE);
            mGifPlayer.fullscreenButton.setVisibility(View.GONE);
            Glide.with(NewsReader.this).load(current_post.getContentImages().get(0)).centerCrop().into(mGifPlayer.thumbImageView);
            //Glide.with(this).load(current_post.getContentImages().get(0)).crossFade().centerCrop().placeholder(R.drawable.photo_placeholder).into(mNewsreaderPoster);
            //posterLoading.setVisibility(View.VISIBLE);
        } else {
            youTubePlayerView.setVisibility(View.GONE);
            mNewsreaderPoster.setVisibility(View.VISIBLE);
            if (current_post.getAttachments().size() > 0) {
                Glide.with(this).load(current_post.getAttachments().get(0).getImages().getFull().getUrl()).crossFade().centerCrop().placeholder(R.drawable.photo_placeholder).into(mNewsreaderPoster);
            } else {
                Glide.with(this).load(current_post.getThumbnail()).crossFade().centerCrop().placeholder(R.drawable.photo_placeholder).into(mNewsreaderPoster);
            }
        }
        youTubePlayerView.getPlayerUIController().setCustomFullScreenButtonListener(view -> {
            String videoId = current_post.getContentImages().get(0).split("ytimg.com/vi/")[1].split("/")[0];
            if (isAppInstalled("com.google.android.youtube")) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + videoId));
                intent.putExtra("VIDEO_ID", videoId);
                intent.putExtra("force_fullscreen", true);
                startActivity(intent);
            } else {
                Toast.makeText(NewsReader.this, "YouTube Not Installed", Toast.LENGTH_LONG).show();
            }
        });
        youTubePlayerView.getPlayerUIController().showCustomAction1(false);
        mNewsCommentCount.setText(""+current_post.getCommentCount());
        KeyboardVisibilityEvent.registerEventListener(this, isOpen -> {
            mNewsreaderSpeak.setVisibility(isOpen?View.GONE:View.VISIBLE);
            mNewsreaderComment.setVisibility(isOpen?View.GONE:View.VISIBLE);
            mNewsreaderShare.setVisibility(isOpen?View.GONE:View.VISIBLE);
            mNewsCommentSend.setVisibility(!isOpen?View.GONE:View.VISIBLE);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.removeRule(RelativeLayout.LEFT_OF);
            params.addRule(RelativeLayout.LEFT_OF, isOpen?mNewsCommentSend.getId():mNewsreaderSpeak.getId());
            mCommentInput.setLayoutParams(params);
        });
        nsv.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (scrollY < 200) {
                if ((int) mNewsGoUp.getTag() == 1) {
                    mNewsGoUp.animate().setDuration(500).translationX(200).start();
                }
                mNewsGoUp.setTag(0);
            } else {
                if ((int) mNewsGoUp.getTag() == 0) {
                        mNewsGoUp.animate().setDuration(500).translationX(0).start();
                }
                mNewsGoUp.setTag(1);
            }
            if (KeyboardVisibilityEvent.isKeyboardVisible(NewsReader.this)) return;
            if (scrollY > (int) nsv.getTag()) {
                if ((int) mCommentBar.getTag() == 1) {
                    mCommentBar.animate().translationY(140).start();
                }
                mCommentBar.setTag(0);
            } else {
                if ((int) mCommentBar.getTag() == 0) {
                    mCommentBar.animate().translationY(0).start();
                }
                mCommentBar.setTag(1);
            }
            nsv.setTag(scrollY);
        });
        mNewsGoUp.setOnClickListener(view -> {
            if((int)mNewsGoUp.getTag()==1) {
                nsv.scrollTo(0,0);
            }
        });

    }
    private String getGifSrc(String src){
        Log.d("giff", "------- "+src);
        String gif=src.split("data-gif=\"")[1].split("\"")[0].trim();
        Log.d("giff", ">> "+gif);
        return  gif;
    }
    private String getVideoSrc(String src){
        String[] mp4d=src.split("/giphy.mp4");
        String video="https://media.giphy.com/media/"+mp4d[mp4d.length-2].split("/media/")[1]+"/giphy.mp4";
        Log.d("giff", ">> "+video);
        return video.trim();
    }
    private boolean isGifPost(Post post){
        for(Category category:post.getCategories()){
            if(category.getSlug().contains("gif")){
                return true;
            }
        }
        return false;
    }
    private boolean isVideoPost(Post post) {
        for (Category category : post.getCategories()) {
            if (category.getSlug().contains("video")) {
                return true;
            }
        }
        return false;
    }

    protected boolean isAppInstalled(String packageName) {
        Intent mIntent = getPackageManager().getLaunchIntentForPackage(packageName);
        if (mIntent != null) {
            return true;
        } else {
            return false;
        }
    }

    private void initFullScreenListener(final YouTubePlayer youTubePlayer) {
        youTubePlayerView.addFullScreenListener(new YouTubePlayerFullScreenListener() {
            @Override
            public void onYouTubePlayerEnterFullScreen() {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                fullScreenManager.enterFullScreen();
                youTubePlayerView.getPlayerUIController().setCustomAction1(ContextCompat.getDrawable(NewsReader.this, R.drawable.ic_pause_36dp), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (youTubePlayer != null) youTubePlayer.pause();
                    }
                });
            }

            @Override
            public void onYouTubePlayerExitFullScreen() {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                fullScreenManager.exitFullScreen();
                youTubePlayerView.getPlayerUIController().showCustomAction1(false);
            }
        });
    }

    private void loadNews() {
        httpClient.get(MainActivity.NEWS_HOST + "?json=get_post&post_id=" + current_post.getId(), new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(NewsReader.this, "load failed, " + throwable.getMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                try {
                    JSONObject jon = new JSONObject(responseString);
                    Post post = gsonb.create().fromJson(jon.getString("post"), Post.class);
                    String sb = "<!DOCTYPE html>" +
                            "<html>" +
                            "  <head>" +
                            "      <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, minimal-ui\" />" +
                            "        <title>Hello Agus Ibrahim</title>" +
                            "    </head>" +
                            "<style type=\"text/css\">" +
                            "body{margin:0;}p{margin: 11px;}" +
                            ".ikn-evt-frame{display:none}" +
                            "em {opacity: 0.4;font-style: italic;}" +
                            "strong {font-size: 20px;margin: 11px;}"
                            + "img{display: inline;height: 200px;width: 100%;}" +
                            "</style>  <body>" + post.getContent().replace("width: 640px;","width: 640px;display:none;").replace("data-gif=","style='display:none;' data-gif=").replace("<p><img ", "<img ").replace("<p><strong><img", "<img").replace("\\n</strong></p>", "\\n</strong>").replace("</em>", "</em><br>") +
                            "  </body>" +
                            "</html>";
                    mNewsreaderContent.loadData(sb, "text/html", "UTF-8");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onDestroy() {
        youTubePlayerView.release();
        super.onDestroy();
    }

    private void initView() {
        mNewsreaderPoster = findViewById(R.id.newsreader_poster);
        mNewsreaderTitle = findViewById(R.id.newsreader_title);
        mNewsreaderDate = findViewById(R.id.newsreader_date);
        mNewsreaderContent = findViewById(R.id.newsreader_content);
        youTubePlayerView = findViewById(R.id.youtube_player_view);
        toolbar = findViewById(R.id.toolbar);
        skeletonView = findViewById(R.id.news_skeleton);
        //toolbarBlur=findViewById(R.id.toolbar_blur);
        nsv = findViewById(R.id.newsreader_nsv);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mYoutubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player_view);
        mNewsreaderPoster = (ImageView) findViewById(R.id.newsreader_poster);
        mNewsreaderTitle = (TextView) findViewById(R.id.newsreader_title);
        mNewsreaderDate = (TextView) findViewById(R.id.newsreader_date);
        mNewsSkeleton = (LinearLayout) findViewById(R.id.news_skeleton);
        mNewsreaderContent = (WebView) findViewById(R.id.newsreader_content);
        mNewsGotoOriginallink = (StateButton) findViewById(R.id.newsreader_goto_originallink);
        mNewsreaderNsv = (NestedScrollView) findViewById(R.id.newsreader_nsv);
        mNewsreaderCommentGrad = (View) findViewById(R.id.newsreader_comment_grad);
        mNewsCommentSend = (ImageView) findViewById(R.id.news_comment_send);
        mCommentBar= findViewById(R.id.newsreader_comment_bar);
        mNewsreaderShare = (ImageView) findViewById(R.id.newsreader_share);
        mNewsreaderComment = (FrameLayout) findViewById(R.id.newsreader_comment);
        mNewsreaderSpeak = (ImageView) findViewById(R.id.newsreader_speak);
        mMainContent = (CoordinatorLayout) findViewById(R.id.main_content);
        mNewsCommentCount=findViewById(R.id.news_comment_count);
        mNewsGoUp= findViewById(R.id.newsreader_goup);
        appBar=findViewById(R.id.appBar);
        posterLoading=findViewById(R.id.newsreader_poster_loading);
        mCommentInput=findViewById(R.id.newsreader_comment_input);
        mGifPlayer=findViewById(R.id.newsreader_gif_player);
        mCommentBar.setTag(1);
        mNewsGoUp.setTag(1);
        nsv.setTag(1);
        mNewsGoUp.animate().setDuration(0).translationX(200).start();
    }
}

package id.rdi.sociarifenews.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import id.rdi.sociarifenews.R;
import id.rdi.sociarifenews.model.news.newspojo.Category;
import id.rdi.sociarifenews.model.news.newspojo.News;
import id.rdi.sociarifenews.model.news.newspojo.Post;

/**
 * Created by RSOS on 12/11/2017.
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.Holdr> {
    public final static int NEWS_NORMAL=1;
    public final static int NEWS_GALERY=2;
    public final static int NEWS_VIDEO=3;
    public final static int NEWS_GIF=4;
    public final static int NEWS_LARGE=5;
    private final Typeface icon_font;
    private List<Post> masterdata;
    private OnNewsClickListener xOnNewsClickListener;
    private OnLoadMoreListener xOnLoadMoreListener;

    public NewsAdapter(List<Post> data, Context ctx){
        this.masterdata=data;
        icon_font= Typeface.createFromAsset(ctx.getAssets(), "fonts/story_icon_assets.ttf");
    }

    @Override
    public Holdr onCreateViewHolder(ViewGroup parent, int viewType) {
        int itemView = 0;
        switch (viewType){
            case NEWS_NORMAL:
                itemView=R.layout.item_news_normal;
                break;
            case NEWS_LARGE:
                itemView=R.layout.item_news_large;
                break;
            case NEWS_GALERY:
                itemView=R.layout.item_news_galery;
                break;
            case NEWS_VIDEO:
                itemView=R.layout.item_news_video;
                break;
            case NEWS_GIF:
                itemView=R.layout.item_news_gif;
                break;
        }
        return new Holdr(LayoutInflater.from(parent.getContext()).inflate(itemView, null));
    }

    private boolean isVideoPost(Post post){
        for(Category category:post.getCategories()){
            if(category.getSlug().contains("video")){
                return true;
            }
        }
        return false;
    }
    private boolean isGifPost(Post post){
        for(Category category:post.getCategories()){
            if(category.getSlug().contains("gif")){
                return true;
            }
        }
        return false;
    }

    @Override
    public int getItemViewType(int position) {
        Post post = masterdata.get(position);
        if(isVideoPost(post)) {
            return NEWS_VIDEO;
        }else if(isGifPost(post)) {
            return NEWS_GIF;
        }else if(post.getContentImages().size()>0){
            return NEWS_GALERY;
        } else if(post.isLarge) return NEWS_LARGE;
        else return NEWS_NORMAL;
    }

    @Override
    public void onBindViewHolder(Holdr holder, int position) {
        Post post = masterdata.get(position);
        if (position == masterdata.size() - 1) {
            if(xOnLoadMoreListener!=null) xOnLoadMoreListener.OnLoadMore(position, masterdata.size());
        }
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            holder.mNewsTitle.setText(Html.fromHtml(post.getTitle(), Html.FROM_HTML_MODE_LEGACY));
        }else{
            holder.mNewsTitle.setText(Html.fromHtml(post.getTitle()));
        }
        holder.mNewsCommentCount.setText(""+post.getCommentCount());
        holder.mNewsCommentCountImg.setTypeface(icon_font);
        if(post.getAttachments().size()>0){
            Log.d("post1", ""+post.getTitle()+" // "+post.getAttachments().get(0).getImages()+".. "+post.getContentImages().size());
            Glide.with(holder.view.getContext()).load(post.isLarge?post.getAttachments().get(0).getImages().getFull().getUrl():post.getAttachments().get(0).getImages().getThumbnail().getUrl()).crossFade().centerCrop().into(holder.mNewsImage);
        }else{
            if(isGifPost(post)){
                holder.news_source.setText(post.getAuthor().getName());
                Glide.with(holder.view.getContext()).load(post.getContentImages().get(0)).crossFade().centerCrop().into(holder.mNewsImage);
            }else {
                Glide.with(holder.view.getContext()).load(post.getThumbnail()).crossFade().centerCrop().into(holder.mNewsImage);
            }
        }
        if(post.getContentImages().size()>0&&!isVideoPost(post)&&!isGifPost(post)){
            switch (post.getContentImages().size()){
                case 1:
                    Glide.with(holder.view.getContext()).load(post.getContentImages().get(0)).placeholder(R.drawable.photo_placeholder).centerCrop().into(holder.img_extra1);
                    Glide.with(holder.view.getContext()).load(post.getThumbnail()).placeholder(R.drawable.photo_placeholder).centerCrop().into(holder.img_extra2);
                    holder.img_extra1.setVisibility(View.VISIBLE);
                    holder.img_extra2.setVisibility(View.VISIBLE);
                    holder.img_extra3.setVisibility(View.GONE);
                    Log.d("loadimg", "url== "+post.getContentImages().get(0));
                    break;
                case 2:
                    Glide.with(holder.view.getContext()).load(post.getContentImages().get(0)).placeholder(R.drawable.photo_placeholder).centerCrop().into(holder.img_extra1);
                    Glide.with(holder.view.getContext()).load(post.getContentImages().get(1)).placeholder(R.drawable.photo_placeholder).centerCrop().into(holder.img_extra2);
                    holder.img_extra1.setVisibility(View.VISIBLE);
                    holder.img_extra2.setVisibility(View.VISIBLE);
                    holder.img_extra3.setVisibility(View.GONE);
                    Log.d("loadimg", "url== "+post.getContentImages().get(0));
                    break;
                default:
                    Glide.with(holder.view.getContext()).load(post.getContentImages().get(0)).placeholder(R.drawable.photo_placeholder).centerCrop().into(holder.img_extra1);
                    Glide.with(holder.view.getContext()).load(post.getContentImages().get(1)).placeholder(R.drawable.photo_placeholder).centerCrop().into(holder.img_extra2);
                    Glide.with(holder.view.getContext()).load(post.getContentImages().get(2)).placeholder(R.drawable.photo_placeholder).centerCrop().into(holder.img_extra3);
                    holder.img_extra1.setVisibility(View.VISIBLE);
                    holder.img_extra2.setVisibility(View.VISIBLE);
                    holder.img_extra3.setVisibility(View.VISIBLE);
                    Log.d("loadimg", "url== "+post.getContentImages().get(0));
                    break;
            }
        }
        //Glide.with(holder.view.getContext()).load(post.getThumbnail()).crossFade().centerCrop().into(holder.mNewsImage);
        //Log.d("post1", ""+post.getTitle()+" // "+post.getAttachments().get(0));
        //Glide.with(holder.view.getContext()).load(post.isLarge?(post.getThumbnailImages()!=null?post.getThumbnailImages().getFull().getUrl():""):post.getThumbnail()).centerCrop().into(holder.mNewsImage);

    }

    public void setOnNewsClickListener(OnNewsClickListener x){
        xOnNewsClickListener=x;
    }
    public void setOnLoadMoreListener(OnLoadMoreListener x){xOnLoadMoreListener=x;}

    public interface OnNewsClickListener{
        void onNewsClick(Post post, int position, Holdr holdr);
    }
    public interface OnLoadMoreListener{
        void OnLoadMore(int position, int count);
    }
    @Override
    public int getItemCount() {
        return masterdata.size();
    }

    public class Holdr extends RecyclerView.ViewHolder{
        View view;
        Holdr root;
        ImageView mNewsImage, img_extra1,img_extra2,img_extra3;
        TextView mNewsTitle;
        TextView mNewsCommentCountImg;
        TextView mNewsCommentCount, news_source;
        LinearLayout mNewsGallery;
        public Holdr(View view) {
            super(view);
            root=this;
            this.view = view;
            this.mNewsImage =  view.findViewById(R.id.news_image);
            this.mNewsTitle =  view.findViewById(R.id.news_title);
            this.mNewsCommentCountImg = view.findViewById(R.id.news_comment_count_img);
            this.mNewsCommentCount = view.findViewById(R.id.news_comment_count);
            this.mNewsGallery=view.findViewById(R.id.news_img_container);
            this.img_extra1=view.findViewById(R.id.news_image_extra1);
            this.news_source=view.findViewById(R.id.news_source);
            this.img_extra2=view.findViewById(R.id.news_image_extra2);
            this.img_extra3=view.findViewById(R.id.news_image_extra3);
            view.setOnClickListener(view1 -> {
                if(xOnNewsClickListener!=null) xOnNewsClickListener.onNewsClick(masterdata.get(getAdapterPosition()), getAdapterPosition(), root);
            });
        }
    }
}
